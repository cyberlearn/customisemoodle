<?php
/**
 * Created by PhpStorm.
 * User: cyril.zufferey
 * Date: 16.04.2018
 * Time: 14:05
 */

defined('MOODLE_INTERNAL') || die();



function getModules($typeMod) {
    global $DB, $CFG;

    $query = "SELECT id, name, visible FROM ".$CFG->prefix.$typeMod;
    $modules = $DB->get_records_sql($query);
    return $modules ;
}
/*
function dataProcessing($activities){
    global $OUTPUT, $DB, $CFG;
    $arrayBack = (array) array();

    foreach($activities as $activity){
        $temp = new stdClass();
        if(file_exists("$CFG->dirroot/mod/$activity->name/lib.php"))
            $temp->name = "<img src=\"" . $OUTPUT->image_url('icon', $activity->name) . "\" class=\"icon\" alt=\"\" />";
        $temp->name .=  get_string('modulename', $activity->name);
        $temp->version = get_config('mod_'. $activity->name, 'version');

        $strshowmodulecourse = get_string('showmodulecourse');
        try {
            $count = $DB->count_records_select($activity->name, "course<>0");
        } catch (dml_exception $e) {
            $count = -1;
        }
        if ($count>0) {
            $countlink = "<a href=\"{$CFG->wwwroot}/course/search.php?modulelist=$activity->name" .
                "&amp;sesskey=".sesskey()."\" title=\"$strshowmodulecourse\">$count</a>";
        } else if ($count < 0) {
            $countlink = get_string('error');
        } else {
            $countlink = "$count";
        }

        $temp->count = $countlink;

        if($activity->visible == 1)
            $temp->visible = '<label class="switchh">'. html_writer::empty_tag('input', array('id' => $activity->id,'type' => 'checkbox', 'name' => 'checkboxActivities', 'checked'=>'checked', 'value' => $activity->id)) . '<span class="sliderr"></span></label>';
        else
            $temp->visible = '<label class="switchh">'. html_writer::empty_tag('input', array('id' => $activity->id,'type' => 'checkbox', 'name' => 'checkboxActivities', 'value' => $activity->id)) . '<span class="sliderr"></span></label>';


        array_push($arrayBack, $temp);
    }

    return $arrayBack;
}
*/


function stateOfPluginActivities($modules, $mforms, $type){

    $work = (array) $mforms;
    $name = '';

    switch($type){
        case "modules":
            $type = "cbmodId_";
            break;
        case "block":
            $type = "cbblockId_";
            break;
        case "assignsubmission" :
            $type = "cbassignsubmissionId_";
            break;
        case "assignfeedback" :
            $type = "cbassignfeedbackId_";
            break;
        case "qtype" :
            $type = "cbqtypeId_";
            break;
        case "qbehaviour" :
            $type = "cbqbehaviourId_";
            break;
        case "editor" :
            $type = "cbeditorId_";
            break;
        case "enrol" :
            $type = "cbenrolId_";
            break;
        case "auth" :
            $type = "cbauthId_";
            break;
        case "media" :
            $type = "cbmediaId_";
            break;
        default:
            return null;
            break;
    }
    foreach ($modules as $module) {
        if(isset($module->name)){
            $name = $module->name;
            $module->plugin = $module->name;
        }
        else{
            $name = $module->plugin;
        }

        if(isset($work[$type . $name])){
            $module->visible = 1;
        }
        else{
            $module->visible = 0;
        }
    }
    return $modules;
}


function show_hide_plugin_assign($plugin, $type) {
    if($plugin->visible == 0){
        set_config('disabled', 1, $type . '_' . $plugin->plugin);
    }
    else{
        set_config('disabled', 0, $type . '_' . $plugin->plugin);

    }
    core_plugin_manager::reset_caches();
}

function show_hide_plugin_qtype($plugin) {
    if($plugin->visible == 0)
        set_config($plugin->plugin . '_disabled', 1, 'question');
    else
        unset_config($plugin->plugin . '_disabled', 'question');
    core_plugin_manager::reset_caches();
}


function show_hide_plugin_qbehaviour($plugins)
{
    $work = array();
    foreach($plugins as $plugin){
        if (!empty(question_engine::is_behaviour_archetypal($plugin->plugin))){
            if($plugin->visible == 0)
                $work[] = $plugin->plugin ;
        }
    }
    set_config('disabledbehaviours', implode(',', $work), 'question');
    core_plugin_manager::reset_caches();
}

function show_hide_plugin_editor($plugins){
    $work = array();
    foreach ($plugins as $plugin){
        if($plugin->visible == 1)
            $work[] = $plugin->plugin;
    }
    if(empty($work))
        $work = array('textarea');

    set_config('texteditors', implode(',', $work));
    core_plugin_manager::reset_caches();
}

function show_hide_plugin_enrol($plugins){
    $work = array();
    foreach ($plugins as $plugin){
        if($plugin->visible == 1)
            $work[] = $plugin->plugin;
    }
    set_config('enrol_plugins_enabled', implode(',', $work));
    core_plugin_manager::reset_caches();
    $syscontext = context_system::instance();
    $syscontext->mark_dirty(); // resets all enrol caches
}

function show_hide_plugin_auth($plugins){
    $work = array();
    foreach ($plugins as $plugin){
        if($plugin->visible === 1)
            $work[] = $plugin->plugin;
    }
    set_config('auth', implode(',', $work));
    \core\session\manager::gc(); // Remove stale sessions.
    core_plugin_manager::reset_caches();
}

function show_hide_plugin_media($modules){
    $plugins = core_plugin_manager::instance()->get_plugins_of_type('media');
    foreach ($modules as $module) {
        if (!is_null($plugins[$module->plugin])) {
            if ($module->visible == 0)
                $plugins[$module->plugin]->set_enabled(false);
            else
                $plugins[$module->plugin]->set_enabled(true);
        }
    }
}

 function get_sorted_plugins_list($type) {
    $names = core_component::get_plugin_list($type);

    $result = array();

    foreach ($names as $name => $path) {
        $idx = get_config($type . '_' . $name, 'sortorder');
        if (!$idx) {
            $idx = 0;
        }
        while (array_key_exists($idx, $result)) {
            $idx +=1;
        }
        $result[$idx] = $name;
    }
    ksort($result);
    return $result;
}


function displayTableState($plugins, $mform){

    foreach ($plugins as $plugin){
        //print_r($plugin);
        if ($plugin->visible == (1 || -1))
            $mform->addElement('html', '<tr><td>');
        else
            $mform->addElement('html', '<tr class="dimmed_text"><td>');

        $mform->addElement('html', get_string('pluginname', $plugin->modtype.'_'.$plugin->plugin) . '</td>');


        switch($plugin->visible){
            case 0 :
                $mform->addElement('html', '<td>');
                $mform->addElement('html', '<input type="checkbox" disabled unchecked>');
                $mform->addElement('html', '</td></tr>');
                break;

            case 1 :
                $mform->addElement('html', '<td>');
                $mform->addElement('html', '<input type="checkbox" disabled checked>');
                $mform->addElement('html', '</td></tr>');
                break;

            case -1 :
            default:
                $mform -> addElement('html', '<td>'.get_string('unchangeable', 'tool_customisemoodle').'</td>');
                break;
        }
    }
}


function displayTable($plugins, $mform){
    global $CFG;
    foreach ($plugins as $plugin){
        //print_r($plugin);
        if ($plugin->visible == (1 || -1))
            $mform->addElement('html', '<tr><td>');
        else
            $mform->addElement('html', '<tr class="dimmed_text"><td>');

        $mform->addElement('html', get_string('pluginname', $plugin->modtype.'_'.$plugin->plugin) . '</td>');


        switch($plugin->visible){
            case 0 :
                $mform->addElement('html', '<td>');
                $mform->addElement('checkbox', 'cb'.$plugin->modtype.'Id_' . $plugin->plugin);
                $mform->setDefault('cb'.$plugin->modtype.'Id_' . $plugin->plugin, 0);
                $mform->addElement('html', '</td></tr>');
                $mform->disabledIf('cb'.$plugin->modtype.'Id_' . $plugin->plugin, 'cb', 'notchecked');
                break;

            case 1 :
                $mform->addElement('html', '<td>');
                $mform->addElement('checkbox', 'cb'.$plugin->modtype.'Id_' . $plugin->plugin);
                $mform->setDefault('cb'.$plugin->modtype.'Id_' . $plugin->plugin, 1);
                $mform->addElement('html', '</td></tr>');
                $mform->disabledIf('cb'.$plugin->modtype.'Id_' . $plugin->plugin, 'cb',  'notchecked');
                break;

            case -1 :
            default:
                $mform -> addElement('html', '<td>'.get_string('unchangeable', 'tool_customisemoodle').'</td>');
                break;
        }
    }
}

function getListPluginTypes(){
    return array('mod', 'assignsubmission', 'assignfeedback', 'block', 'qtype', 'qbehaviour', 'editor', 'enrol', 'auth', 'media');
}

function performChanges($typeList, $fromform){

    foreach ($typeList as $type){
        switch($type){
            case 'modules' :
            case 'mod' :
                $thistype = "modules";
                $activities = getModules($thistype);
                $modules = stateOfPluginActivities($activities, $fromform, $thistype);
                //print_r($modules); die();
                changeStatesInDB($modules, $thistype);
                break;

            case 'block' :
                $blocks = getModules($type);
                $modules = stateOfPluginActivities($blocks, $fromform, $type);
                changeStatesInDB($modules, $type);
                break;

            case 'assignsubmission' :
            case 'assignfeedback' :
                $plugins = get_sorted_plugins_list($type);
                $assigns = array();
                foreach ($plugins as $plugin){
                    $work = new stdClass();
                    $work->plugin = $plugin;
                    $work->visible = !get_config($type . '_' . $plugin, 'disabled');
                    array_push($assigns, $work);
                }
                $modules = stateOfPluginActivities($assigns, $fromform, $type);
                changeStatesInDB($modules, $type);
                break;

            case 'qtype' :
                $plugins = get_sorted_plugins_list($type);
                $createabletypes = question_bank::get_creatable_qtypes();
                $qtypes = array();
                foreach($plugins as $plugin){
                    $work = new stdClass();
                    $work->plugin = $plugin;
                    $createable = isset($createabletypes[$plugin]);
                    if($createable === 1)
                        $work->visible = $createable;
                    else
                        $work->visible = 0;

                    if($plugin !== "random" || $plugin !== "missingtype"){
                        array_push($qtypes, $work);
                    }
                }
                $modules = stateOfPluginActivities($qtypes, $fromform, $type);
                changeStatesInDB($modules, $type);
                break;

            case 'qbehaviour' :
            case 'enrol' :
            case 'editor' :
            case 'auth' :
            case 'media' :
                $plugins = get_sorted_plugins_list($type);
                $qplugins = array();
                foreach($plugins as $plugin){
                    $work = new stdClass();
                    $work->plugin = $plugin;
                    $work->visible = null;
                    array_push($qplugins, $work);
                }
                $modules = stateOfPluginActivities($qplugins, $fromform, $type);
                changeStatesInDB($modules, $type);
                break;
        }

    }
}

function changeStatesInDB($modules, $type){
    global $DB;

    switch($type){
        case "modules" :
        case "mod" :
            foreach($modules as $module){
                if(isset($module->name))
                    $conditions = array("name" => $module->name);
                else
                    $conditions = array("name" => $module->plugin);
                $res = $DB->get_record('modules', $conditions, 'id');

                if($module->name !== 'forum'){
                    $query = "UPDATE {modules} SET visible=? WHERE id = ? " ;
                    $param = array($module->visible, $res->id);
                    $DB->execute($query, $param);
                    $query = "UPDATE {course_modules} SET visibleold=visible, visible=? WHERE module = ? " ;
                    $DB->execute($query, $param);
                }
            }
            break;

        case "block" :
            foreach($modules as $module){
                if(isset($module->name))
                    $conditions = array("name" => $module->name);
                else
                    $conditions = array("name" => $module->plugin);
                $res = $DB->get_record('block', $conditions, 'id');

                $query = "UPDATE {block} SET visible=? WHERE id = ? " ;
                $param = array($module->visible, $res->id);
                $DB->execute($query, $param);
            }
            break;

        case "assignsubmission" :
        case "assignfeedback" :
            foreach($modules as $module)
                show_hide_plugin_assign($module, $type);
            break;

        case "qtype" :
            foreach($modules as $module)
                show_hide_plugin_qtype($module);
            break;
        case "qbehaviour" :
            show_hide_plugin_qbehaviour($modules);
            break;
        case 'editor' :
            show_hide_plugin_editor($modules);
            break;
        case 'enrol' :
            show_hide_plugin_enrol($modules);
            break;
        case 'auth' :
            show_hide_plugin_auth($modules);
            break;
        case 'media' :
            show_hide_plugin_media($modules);
            break;
    }


   // echo $string;
}

function isCustomiseTableEmpty(){
    global $DB;
    if($DB->count_records('tool_customisemoodle_save', $conditions=null) > 0)
        return false;

    return true;
}

function saveStates($states){
    global $DB;
    foreach ($states as $state){
        if($state->visible == -1)
            $state->visible = 1;
        $sql = 'INSERT INTO {tool_customisemoodle_save} (`plugin`, `modtype`, `visible`) VALUES(:v1, :v2, :v3)';
        $params = ['v1' => $state->plugin, 'v2' => $state->modtype, 'v3' => $state->visible];
        $DB->execute($sql, $params);
    }
}

function deleteStates(){
    global $DB;
    $sql = 'DELETE FROM {tool_customisemoodle_save}';
    $DB->execute($sql, null);}

?>
