<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Performs checkout of the strings into the translation table
 *
 * @package    tool
 * @subpackage customlang
 * @copyright  2010 David Mudrak <david@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('NO_OUTPUT_BUFFERING', true); // progress bar is used here

require(__DIR__ . '/../../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require("classes/custom_form.php");
require("localib.php");

echo '<script src="https://code.jquery.com/jquery-1.10.2.js"></script>';
echo '<link rel="stylesheet" href="customisesheet.css" type="text/css">';

require_login();

admin_externalpage_setup('tool_customisemoodle');


// output starts here
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pluginname', 'tool_customisemoodle'));

$mformDefaultSelector = new mod_selector_form();

//Form processing and displaying is done here
if ($mformDefaultSelector->is_cancelled()) {
    $returnurl = new moodle_url('/admin/tool/customisemoodle/index.php');
    redirect($returnurl);
} else if ($fromform = $mformDefaultSelector->get_data()) {

    if(!empty($fromform->savestates)){
        if(isCustomiseTableEmpty()){
            saveStates($mformDefaultSelector->getStates());
        }
        else{
            deleteStates();
            saveStates($mformDefaultSelector->getStates());
        }
    }

    $typeList = getListPluginTypes();
    performChanges($typeList, $fromform);

    $returnurl = new moodle_url('/admin/tool/customisemoodle/visibility.php');
    redirect($returnurl);

    //In this case you process validated data. $mform->get_data() returns data posted in form.
} else {
    // this branch is executed if the form is submitted but the data doesn't validate and the form should be redisplayed
    // or on the first display of the form.

    //Set default data (if any)
    //$mformDefaultSelector->set_data($toform);
    //displays the form
    $mformDefaultSelector->display();
}




echo $OUTPUT->footer();

?>

