<?php
// This file is part of
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.



defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir . '/questionlib.php');


class mod_selector_form extends moodleform
{
    private $backstates;
    //Add elements to form
    public function definition()
    {
        global $CFG, $DB, $OUTPUT;

        $mform = $this->_form; // Don't forget the underscore!


        $pluginTypes = (array) getListPluginTypes();

        $mform->addElement('html', '<table class="generaltable" style="width: 50%; margin-left: auto; margin-right: auto;">');
        $states = array();
        foreach($pluginTypes as $type){
            $mform->addElement('html', '<tr><th colspan="2">'.$OUTPUT->heading(get_string('plugin_'.$type, 'tool_customisemoodle')).'</th></tr>');
            $mform->addElement('html', '<tr><th>'.get_string('plugin', 'tool_customisemoodle').'</th><th>'.get_string('hideshow', 'tool_customisemoodle').'</th></tr>');
            $myvar = $this->display_plugin_table($type);
            if($myvar === false)
                $mform->addElement('html', '<tr><td colspan="2">'.get_string('noentry', 'tool_customisemoodle').'</td></tr>');
            else
                displayTable($myvar, $mform);

            $mform->addElement('html', '<tr class="blank_row"><th colspan="2"></th></tr>');

            foreach($myvar as $item)
                array_push($states, $item);
        }
        $mform->addElement('html', '</table>');

        echo '<br><br><br>';

        $this->setStates($states);

        $mform->addElement('checkbox', 'savestates', get_string('savestates', 'tool_customisemoodle'));

        $this->add_action_buttons();
    }

    //Custom validation should be added here
    function validation($data, $files)
    {
        return array();
    }

    private function setStates($states){
        $this->backstates = $states;
    }

    function getStates(){
        return $this->backstates;

    }


    function display_plugin_table($type){
        $pluginNames = get_sorted_plugins_list($type);

        if (empty(($pluginNames)))
            return false;

        return $this->view_plugins_table($pluginNames, $type);
    }




    private function view_plugins_table($plugins, $type) {
        global $DB, $CFG;

        $arrayBack = array();


        foreach ($plugins as $idx => $plugin) {
            $row = new stdClass();
            $row->plugin = $plugin;
            $row->modtype = $type;

            switch($type){
                case 'mod' :
                case 'block' :
                    if($type === 'mod')
                        $typeName = 'modules';
                    else
                        $typeName = 'block';

                    $visible = $this->get_visibility($plugin, $typeName);
                    $visible = $visible->visible;
                    if($plugin === 'forum')
                        $visible = -1;
                    break;

                case 'assignsubmission' :
                case 'assignfeedback' :
                    $visible = !get_config($type . '_' . $plugin, 'disabled');
                    break;

                case 'qtype' :
                    $createabletypes = question_bank::get_creatable_qtypes();
                    $createable = isset($createabletypes[$plugin]);
                    if($createable == 1){
                        $visible = $createable;
                    }
                    else
                        $visible = 0;

                    if($plugin == "random" || $plugin == "missingtype")
                        $visible = -1;
                    break;

                case 'qbehaviour' :
                    $config = get_config('question');

                    if (!empty($config->disabledbehaviours)) {
                        $disabledbehaviours = explode(',', $config->disabledbehaviours);
                    }
                    $key = array_search($plugin, $disabledbehaviours);
                    if($disabledbehaviours[$key] !== $plugin)
                        $visible = 1;
                    else
                        $visible = 0;
                    if(!isset($visible) || empty(question_engine::is_behaviour_archetypal($plugin)))
                        $visible = -1;
                    break;

                case 'editor' :
                case 'enrol' :
                case 'auth' :
                case 'media' :
                    $function = '\core\plugininfo\\' .$type.'::get_enabled_plugins';
                    $enabled = array_values((call_user_func($function)));
                    $key = array_search($plugin, $enabled);
                    if($enabled[$key] === $plugin)
                        $visible = 1;
                    else
                        $visible = 0;
                    break;

            }

            switch($visible){
                case 1 :
                    $row->visible = $visible;
                    break;
                case -1:
                    $row->visible = -1;
                    break;
                case 0 :
                default:
                    $row->visible = 0;
                    break;
            }



            array_push($arrayBack, $row);


        }
        return $arrayBack;
    }

    private function get_visibility($plugin, $type){
        global $DB, $CFG;
        $query = "SELECT visible FROM ".$CFG->prefix.$type." WHERE name LIKE '".$plugin."'";
        $modules = $DB->get_record_sql($query);
        return $modules ;
    }

}
?>




