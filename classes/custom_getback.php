<?php
// This file is part of
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir . '/questionlib.php');


class mod_getback_form extends moodleform
{
    //Add elements to form
    public function definition()
    {
        global $CFG, $DB, $OUTPUT;

        $mform = $this->_form;

        echo '<br><br><br>';
        $buttonarray=array();
        $buttonarray[] =& $mform->createElement('submit', 'getback', get_string('getbacksure', 'tool_customisemoodle'));
        $buttonarray[] =& $mform->createElement('submit', 'cancel', get_string('cancel'));
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);


        $mform->addElement('html', '<br><br><br>');
        $mform->addElement('html', '<h2>'. get_string('titlestatebackup', 'tool_customisemoodle') .'</h2>');
        $mform->addElement('html', '<p>'. get_string('txtstatebackup', 'tool_customisemoodle') .'</p>');

        if($count = $DB->count_records('tool_customisemoodle_save', $conditions=null) > 0){
            $pluginTypes = getListPluginTypes();
            $mform->addElement('html', '<table class="generaltable" style="width: 50%; margin-left: auto; margin-right: auto;">');
            foreach($pluginTypes as $type){
                $conditions = array('modtype' => $type);
                $plugins = $DB->get_records('tool_customisemoodle_save', $conditions);
                $mform->addElement('html', '<tr><th colspan="2">'.$OUTPUT->heading(get_string('plugin_'.$type, 'tool_customisemoodle')).'</th></tr>');
                $mform->addElement('html', '<tr><th>'.get_string('plugin', 'tool_customisemoodle').'</th><th>'.get_string('hideshow', 'tool_customisemoodle').'</th></tr>');
                displayTableState($plugins, $mform);
                $mform->addElement('html', '<tr class="blank_row"><th colspan="2"></th></tr>');
            }
            $mform->addElement('html', '</table>');
        }
        else{
            $mform->addElement('html', '<div class="alert alert-danger" role="alert">'. get_string('nodata', 'tool_customisemoodle') .'</div>');
            $mform->disabledIf('getback', false, 'eq', false);
        }
    }

    //Custom validation should be added here
    function validation($data, $files)
    {
        return array();
    }
}