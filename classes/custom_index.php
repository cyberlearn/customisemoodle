<?php
// This file is part of
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Step 1 form.
 *
 * @package    tool_coursearchiver
 * @copyright  2015 Matthew Davidson
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir . '/questionlib.php');

class mod_index_form extends moodleform
{
    //Add elements to form
    public function definition()
    {
        global $CFG, $DB, $OUTPUT;

        $mform = $this->_form;

        echo '<br><br><br>';
        $buttonarray=array();
        $buttonarray[] =& $mform->createElement('submit', 'setvisibility', get_string('setvisibility', 'tool_customisemoodle'));
        $buttonarray[] =& $mform->createElement('submit', 'cancel', get_string('getback', 'tool_customisemoodle'));
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);

    }

    //Custom validation should be added here
    function validation($data, $files)
    {
        return array();
    }
}