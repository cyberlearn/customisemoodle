<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     tool_customisemoodle
 * @category    string
 * @copyright   2018 Cyberlearn <cyberlearn@hes-so.ch>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['en'] = 'English';
$string['pluginname'] = 'Customise Moodle';
$string['3choices'] = 'You have three different ways to customise your Moodle website. Each mode will activate or deactivate specific Moodle plugins which are installed on your Moodle website. At any time it will be possible to get back to the inital state or to change the current state.';
$string['choices'] = "Welcome to the Customise's plugin. You have two options to use this plugin <br><br><ul><li>If you wish to <b>set the visibility</b> of the Moodle's pluings click on the '<i>Define the plugins' visibility !</i>' button.</li> <li>If you wish to <b>get back to the previous state</b> of visibility press the '<i>Get back to the previous configuration !</i>' button.</li></ul>";
$string['choice1'] = 'Default installation - Moodle lite';
$string['choice1_explanation'] = 'This version of Moodle will only let you use the ';
$string['choice2'] = 'Full customisation';
$string['choice3'] = 'Back to the original';
$string['unchangeable'] = 'Unchangeable';

$string['setvisibility'] = "Define the plugins' visibility !";
$string['getback'] = 'Get back to the previous configuration !  ';
$string['getbacksure'] = 'Sure, get back to the previous configuration !';


$string['plugin'] = 'Plugin';
$string['hideshow'] = 'Hide/Show';
$string['noentry'] = 'Sorry, it seems that there is no plugin available for this type of plugin.';


$string['plugin_name'] = 'Plugin name';
$string['plugin_mod'] = 'Activity module';
$string['plugin_block'] = 'Block module';
$string['plugin_assignsubmission'] = 'Assignment / Submission plugins';
$string['plugin_assignfeedback']  = 'Assignment / Feedback plugins';
$string['plugin_qtype']  = 'Question types';
$string['plugin_qbehaviour']  = 'Question behaviours';
$string['plugin_editor']  = 'Editors';
$string['plugin_enrol']  = 'Enrolment methods';
$string['plugin_auth']  = 'Authentication methods';
$string['plugin_media']  = 'Media players';


$string['version_mod'] = 'Version';
$string['count_mod'] = 'Count';
$string['hide_show'] = 'Hide/Show';

$string['savestates'] = "By checking this box, you create a visibility backup of the above plugins in the state they were before your changes. This measure will allow you to find the initiale situation in case of need. <br>However, be aware that if you have already made a backup, it will be automatically deleted and replaced by this version.";

$string['getbacksuretxt'] = "By clicking the 'Get back' button, the previsous states will be restored ! If you do not want to restore them press 'Cancel'";
$string['titlestatebackup'] = 'State of the backup';
$string['txtstatebackup'] = "Below you find the state of the backup. By clicking on the 'Get back' button, the visibility of the plugins will be identical to what is presented below.";
$string['nodata'] = '<strong>Sorry !</strong> There is no data saved. First you have to save the visibility of the Moodle\'s pluings';

